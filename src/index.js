require('dotenv').config();

const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb+srv://admin:admin123@katerynakyrylovamongodb.4rljmgg.mongodb.net/notes?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);

const port = process.env.PORT || 8080;

const start = async () => {
  try {
    if (!fs.existsSync('notes')) {
      fs.mkdirSync('notes');
    }
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

function errorHandler(err, req, res) {
  console.error('err');
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
